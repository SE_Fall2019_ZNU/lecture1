# social (java hello world)
> A simple java hello world web application with one simple use case for showing the structure of a web application

## Table of contents
* [Use Cases](#general-info)
* [Technologies](#technologies)
* [Further Reading](#further-reading)

## Use Cases
* User can enter an URL with a post ID and view that post with the corresponding comments

## Technologies
* Java Servlets &  JSP
* HikariCP for connection pooling
* SQL Server for DB

## Further Reading
* ##### Connection Pooling
    * [The Anatomy of Connection Pooling](https://vladmihalcea.com/the-anatomy-of-connection-pooling/)
    * [Java Connection Pooling](https://www.baeldung.com/java-connection-pooling)
* ##### Singleton
    * [Singleton Design Pattern](https://www.geeksforgeeks.org/singleton-design-pattern/)
    * [Singleton vs Static](https://stackoverflow.com/questions/519520/difference-between-static-class-and-singleton-pattern)
* ##### Dependency Injection
    * [A quick intro to Dependency Injection: what it is, and when to use it](https://www.freecodecamp.org/news/a-quick-intro-to-dependency-injection-what-it-is-and-when-to-use-it-7578c84fa88f/)
    