package controller;

import domain.Post;
import repository.PostDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GetPost", urlPatterns = "/GetPost")
public class GetPost extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher view;

        try {
            PostDAO postDAO = new PostDAO(); // Coupling between this controller and the implementation of PostDAO(PostDAO should be an interface and the right implementation should be injected into this controller)
            Post post = postDAO.getPostByID(Long.valueOf(req.getParameter("id").toString()));

            if(post != null) {
                req.setAttribute("post", post);
                view = req.getRequestDispatcher("post.jsp");
            } else {
                view = req.getRequestDispatcher("404.jsp");
            }
        } catch (Exception e) {
            e.printStackTrace();
            view = req.getRequestDispatcher("500.jsp");
        }

        view.forward(req, resp);
    }
}
