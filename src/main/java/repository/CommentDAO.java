package repository;

import domain.Comment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentDAO {
    private ConnectionManager connectionManager;
    public CommentDAO() throws ClassNotFoundException {
        connectionManager = ConnectionManager.getInstance();
    }

    public List<Comment> getCommentsOfPost(long postId) {
        List<Comment> result = null;
        try (Connection connection = connectionManager.getConnection()){
            String query = "SELECT * FROM Comment WHERE post_id=?";
            PreparedStatement prest = connection.prepareStatement(query);
            prest.setLong(1, postId);
            ResultSet rs = prest.executeQuery();
            result = new ArrayList<>();
            while (rs.next()) {
                Comment comment = new Comment();
                comment.setId(rs.getLong("id"));
                comment.setUsername(rs.getString("username"));
                comment.setText(rs.getString("text"));

                result.add(comment);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
}
