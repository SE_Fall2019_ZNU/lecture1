package repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

class ConnectionManager {
    private static ConnectionManager instance;
    private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;

    public static ConnectionManager getInstance() throws ClassNotFoundException {
        if(instance == null) {
            instance = new ConnectionManager();
        }
        return instance;
    }


    private ConnectionManager() throws ClassNotFoundException {
        config.setJdbcUrl("jdbc:sqlserver://localhost:1433;DatabaseName=Social");
        config.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        config.setUsername("sa");
        config.setPassword("123456");
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        ds = new HikariDataSource(config);
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
