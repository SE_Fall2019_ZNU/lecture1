package repository;

import domain.Post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PostDAO {
    private ConnectionManager connectionManager;
    public PostDAO() throws ClassNotFoundException {
        connectionManager = ConnectionManager.getInstance();
    }

    public Post getPostByID(long id) {
        Post result = null;
        try (Connection connection = connectionManager.getConnection()){
            String query = "SELECT * FROM Post WHERE id=?";
            PreparedStatement prest = connection.prepareStatement(query);
            prest.setLong(1, id);
            ResultSet rs = prest.executeQuery();
            if (rs.next()) {
                result = new Post();
                result.setId(id);
                result.setText(rs.getString("text"));
                // Uses two connection for one operation which is not efficients
                result.setComments(new CommentDAO().getCommentsOfPost(id));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }
}
