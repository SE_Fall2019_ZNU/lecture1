<%@ page import="domain.Post" %><%--
  Created by IntelliJ IDEA.
  User: amir
  Date: 10/12/2019
  Time: 10:24 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Post</title>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<% Post post = ((Post) request.getAttribute("post"));%>

<h3>Text</h3>
<%=post.getText()%>

<br />
<h3>
    Comments:
</h3>

<table>
    <tr>
        <th>Username</th>
        <th>Text</th>
    </tr>
    <% for(int i = 0; i < post.getComments().size(); i+=1) { %>
    <tr>
        <td><%=post.getComments().get(i).getUsername()%></td>
        <td><%=post.getComments().get(i).getText()%></td>
    </tr>
    <% } %>
</table>

</body>
</html>
